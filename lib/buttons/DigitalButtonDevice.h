#ifndef DigitalButtonDevice_h
#define DigitalButtonDevice_h

#include <Arduino.h>

class DigitalButtonDevice
{      
    public:        
        DigitalButtonDevice(int pin);
        bool pushed();
    private:
        int pin; 
        int state;
        int read_state();
};

#endif // DigitalButtonDevice_h
