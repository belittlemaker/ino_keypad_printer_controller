#include "DigitalButtonDataDevice.h"

DigitalButtonDataDevice::DigitalButtonDataDevice(int pin, String label_, String data_) : DigitalButtonDevice(pin)
{
    this->label_ = label_;
    this->data_ = data_;
}

DigitalButtonDataDevice::DigitalButtonDataDevice(int pin, String label_) : DigitalButtonDevice(pin)
{
    this->label_ = label_;
    this->data_ = "";
}

String DigitalButtonDataDevice::get_label()
{
    return this->label_;
}

String DigitalButtonDataDevice::get_data()
{
    return this->data_;
}

String DigitalButtonDataDevice::get_output()
{
    String output = (String)PREFIX_OUTPUT + " ";
    
    if (this->get_data() == "") {
        output += this->get_label();
    } else {
        output += this->get_label() + " " + this->get_data();
    }

    return output;
}
