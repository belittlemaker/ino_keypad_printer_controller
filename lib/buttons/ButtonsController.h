#ifndef ButtonsController_h
    #define ButtonsController_h
  
    #include "AbstractOutputPipe.h"
    #include "ButtonsDefines.h"
    #include "DigitalButtonDataDevice.h"
    
    class ButtonsController
    {      
        public:        
            ButtonsController(AbstractOutputPipe* pipe);
            void check_pushed_buttons();
        private:
            AbstractOutputPipe* pipe;
            DigitalButtonDataDevice* buttons[8];
    };

#endif // ButtonsController_h
