#include "DigitalButtonDevice.h"

DigitalButtonDevice::DigitalButtonDevice(int pin)
{
    this->pin = pin;
    this->state = LOW;
}

bool DigitalButtonDevice::pushed()
{
    int state_new = this->read_state();

    bool pushed = state_new == HIGH && this->state == LOW;
    
    this->state = state_new;

    return pushed;
}

int DigitalButtonDevice::read_state()
{
    return digitalRead(this->pin);
}
