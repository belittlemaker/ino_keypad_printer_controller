#include "ButtonsController.h"

ButtonsController::ButtonsController(AbstractOutputPipe* pipe)
{
    // row 1
    buttons[0] = new DigitalButtonDataDevice(PIN_CONENCT, "connect");
    buttons[1] = new DigitalButtonDataDevice(PIN_HOME, "home");
    buttons[2] = new DigitalButtonDataDevice(PIN_PAUSE, "pause");
    buttons[3] = new DigitalButtonDataDevice(PIN_CANCEL, "cancel");
    // row 2
    buttons[5] = new DigitalButtonDataDevice(PIN_LEVEL, "level");
    buttons[4] = new DigitalButtonDataDevice(PIN_COLD, "cold");
    buttons[6] = new DigitalButtonDataDevice(PIN_HOT_BED, "hot_bed", (String)60);
    buttons[7] = new DigitalButtonDataDevice(PIN_HOT_TOOL, "hot_tool", (String)210);

    this->pipe = pipe;
}

void ButtonsController::check_pushed_buttons()
{
    for (DigitalButtonDataDevice* button : this->buttons) {
        if (button->pushed()) {
            this->pipe->write(button->get_output());
            break;
        }
    }
}
