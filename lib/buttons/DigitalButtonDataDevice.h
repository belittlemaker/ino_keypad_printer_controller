#ifndef DigitalButtonDataDevice_h
    #define DigitalButtonDataDevice_h

    #include "DigitalButtonDevice.h"
    #include "ButtonsDefines.h"
    
    class DigitalButtonDataDevice : public DigitalButtonDevice
    {      
        public:        
            DigitalButtonDataDevice(int pin, String label_, String data_);
            DigitalButtonDataDevice(int pin, String label_);
            String get_label();
            String get_data();
            String get_output();
        private:
            String label_;
            String data_;
    };

#endif // DigitalButtonDataDevice_h
