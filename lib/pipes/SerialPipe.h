#ifndef SerialPipe_h
#define SerialPipe_h

#include <Arduino.h>
#include "AbstractInputPipe.h"
#include "AbstractOutputPipe.h"

class SerialPipe : public AbstractInputPipe, public AbstractOutputPipe
{      
    public:
        SerialPipe(int baud);
        ~SerialPipe();
        char read();
        void write(String s);
};

#endif // SerialPipe_h