#ifndef AbstractInputPipe_h
#define AbstractInputPipe_h

#include <Arduino.h>

class AbstractInputPipe
{      
    public:
        virtual char read() = 0;
};

#endif // AbstractInputPipe_h