#include "SerialPipe.h"

SerialPipe::SerialPipe(int baud)
{
    Serial.begin(baud);
    Serial.flush();
}

SerialPipe::~SerialPipe()
{
    Serial.end();
}

char SerialPipe::read()
{
    return Serial.read();
}

void SerialPipe::write(String s)
{
    Serial.println(s);
}
