#ifndef AbstractOutputPipe_h
#define AbstractOutputPipe_h

#include <Arduino.h>

class AbstractOutputPipe
{      
    public:
        virtual void write(String s) = 0;
};

#endif // AbstractOutputPipe_h