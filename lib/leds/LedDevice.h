#ifndef LedDevice_h
#define LedDevice_h

#include <Arduino.h>

class LedDevice
{      
    public:        
        LedDevice(int pin);
        void switch_on();
        void switch_off();
    private:
        int pin; 
};

#endif // LedDevice_h
