#include "LedsController.h"

LedsController::LedsController(AbstractInputPipe* pipe)
{
    this->led_offline = new LedDevice(PIN_OFFLINE);
    this->led_online = new LedDevice(PIN_ONLINE);
    
    this->pipe = pipe;
}

void LedsController::check_serial_input()
{
    if (Serial.available()) {
        char serial_input = pipe->read();

        if (serial_input == PRINTER_OFFLINE) {
            this->printer_offline();
        } else if (serial_input == PRINTER_ONLINE) {
            this->printer_online();
        } else if (serial_input == PRINTER_EXCEPTION) {
            this->printer_exception();
        }
    }
}

void LedsController::printer_exception()
{
    for (int i = 0; i <= 2  ; i++) {
        led_offline->switch_on();
        delay(500);
        led_offline->switch_off();
        delay(500);
    }
}

void LedsController::printer_offline()
{
    led_online->switch_off();
    led_offline->switch_on();
}

void LedsController::printer_online()
{
    led_offline->switch_off();
    led_online->switch_on();
}
