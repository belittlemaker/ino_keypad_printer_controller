#include "LedDevice.h"

LedDevice::LedDevice(int pin)
{
    this->pin = pin;
    
    pinMode(this->pin, OUTPUT);

    this->switch_off();
}

void LedDevice::switch_on()
{
    digitalWrite(this->pin, HIGH);
}

void LedDevice::switch_off()
{
    digitalWrite(this->pin, LOW);
}
