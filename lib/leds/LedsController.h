#ifndef LedsController_h
    #define LedsController_h
  
    #include "AbstractInputPipe.h"
    #include "LedsDefines.h"
    #include "LedDevice.h"
    
    class LedsController
    {      
        public:        
            LedsController(AbstractInputPipe* pipe);
            void check_serial_input();
        private:
            AbstractInputPipe* pipe;
            LedDevice* led_offline;
            LedDevice* led_online;
            void printer_exception();
            void printer_offline();
            void printer_online();
    };

#endif // LedsController_h
