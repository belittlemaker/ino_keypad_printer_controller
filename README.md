# ino-keypad-printer-controller
Arduino controller for the keyboard

![](assets/img/keypad.png)

Project
-------
- Build in Platformio for Visual Studio Code
  - https://platformio.org/install/ide?install=vscode
- Circuit diagram
  - https://www.tinkercad.com/things/0Ch88tQTGJb-analog-switch-board

![](assets/img/diagram.png)

Steps before
------------
Review the configuration files & choose your own configuration:
- Buttons pins
  - lib/buttons/ButtonsDefines.h
- Leds pins
  - lib/leds/LedsDefines.h

Functionality
-------------
- Leds by Serial input
  - '_1_': green led on
  - '_2_': red led on
  - '_3_': red led blinking
- Buttons pushed -> Serial output
  - Row 1: '_connect_', '_home_', '_pause_', '_cancel_'
  - Row 2: '_level_', '_cold_', '_hot_bed_', '_hot_tool_'
