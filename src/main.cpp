#include <Arduino.h>
#include "ButtonsController.h"
#include "LedsController.h"
#include "SerialPipe.h"

SerialPipe *serial_pipe;
ButtonsController *buttons_controller;
LedsController *leds_controller;

void setup()
{
    serial_pipe = new SerialPipe(9600);
    buttons_controller = new ButtonsController(serial_pipe);
    leds_controller = new LedsController(serial_pipe);
}

void loop()
{
    buttons_controller->check_pushed_buttons();
    leds_controller->check_serial_input();
}